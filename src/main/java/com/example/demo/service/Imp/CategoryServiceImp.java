package com.example.demo.service.Imp;

import com.example.demo.Utilities.Pagination;
import com.example.demo.repository.BookRepository;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.DTO.Book;
import com.example.demo.repository.DTO.Category;

import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryServiceImp implements CategoryService {

    private CategoryRepository categoryRepository;
    @Autowired
    public void setBookRepository(CategoryRepository categoryRepository){
        this.categoryRepository=categoryRepository;
    }
    @Override
    public Category insert(Category category) {
        boolean isInserted =categoryRepository.insert(category);
        if(isInserted)
            return category;
        else
            return null;
    }

    @Override
    public List<Category> getData(Pagination pagination) {
        return categoryRepository.getData(pagination);
    }

    @Override
    public Category update(Category category) {
        boolean isUpdated =categoryRepository.update(category);
        if(isUpdated)
            return category;
        else
            return null;
    }


    @Override
    public int countAllCategory() {
        return categoryRepository.coutAllCategories();
    }

    @Override
    public boolean delete(int id) {
        return categoryRepository.delete(id);
    }

    @Override
    public Category findid(int id) {
        return categoryRepository.findid(id);
    }

    @Override
    public Category getbyid(int id) {
        return categoryRepository.getbyid(id);
    }
    @Override
    public Category getbytitle(String title) {
        return categoryRepository.getbytitle(title);
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }
}
