package com.example.demo.service.Imp;

import com.example.demo.Utilities.Pagination;
import com.example.demo.repository.BookRepository;
import com.example.demo.repository.DTO.Book;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService
{

    private BookRepository bookRepository;
    @Autowired
    public void setBookRepository(BookRepository bookRepository){
        this.bookRepository=bookRepository;
    }
    @Override
    public Book insert(Book book) {
        boolean isInserted =bookRepository.insert(book);
        if(isInserted)
            return book;
        else
            return null;
    }

    @Override
    public List<Book> getData(Pagination pagination) {
        return bookRepository.getData(pagination);
    }

    @Override
    public Book update(Book book) {
        boolean isUpdated =bookRepository.update(book);
        if(isUpdated)
            return book;
        else
            return null;
    }


    @Override
    public int coutAllBook() {
        return bookRepository.coutAllBook();
    }

    @Override
    public boolean delete(int id) {
        return bookRepository.delete(id);
    }

    @Override
    public Book findid(int id) {
        return bookRepository.findid(id);
    }

    @Override
    public Book getbyid(int id) {
        return bookRepository.getbyid( id);
    }
    @Override
    public Book getbytitle(String title) {
        return bookRepository.getbytitle(title);
    }

    @Override
    public List<Book> findAll() {
        return bookRepository.findAll();
    }
}
