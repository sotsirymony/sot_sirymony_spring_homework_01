package com.example.demo.service;

import com.example.demo.Utilities.Pagination;
import com.example.demo.repository.DTO.Book;
import com.example.demo.repository.DTO.Category;

import java.util.List;

public interface CategoryService
{

    Category insert(Category category);
    List<Category> getData(Pagination pagination);
    Category update(Category category);
    boolean delete(int id);
    Category findid(int id);
    Category getbyid(int id);
    Category getbytitle(String title);
    int countAllCategory();
    List<Category> findAll();

}