package com.example.demo.service;

import com.example.demo.Utilities.Pagination;
import com.example.demo.repository.DTO.Book;

import java.util.List;

public interface BookService
{

    Book insert(Book article);
    List<Book> getData(Pagination pagination);
    Book update(Book article);
    boolean delete(int id);
    Book findid(int id);
    Book getbyid(int id);
    Book getbytitle(String title);
    int coutAllBook();
    List<Book> findAll();

}