package com.example.demo.restcontroller;

import com.example.demo.Utilities.Pagination;
import com.example.demo.repository.DTO.Book;
import com.example.demo.repository.DTO.Category;
import com.example.demo.service.BookService;
import com.example.demo.service.CategoryService;
import com.example.demo.service.Imp.CategoryServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryRestController {

    private CategoryService categoryService;
    @Autowired
    public void setArticleService(CategoryService  categoryService)
    {
        this.categoryService = categoryService;
    }

    @RequestMapping(method= RequestMethod.GET)
    public Map<String,Object> getArticle(){
        Map<String,Object> result=new HashMap<>();
        List<Category> category=categoryService.findAll();
        if(category.isEmpty()){
            result.put("Message","No data in database");
        }else{
            result.put("Message","get seccessfully");
        }
        result.put("DATA",category);
        return result;
    }

@RequestMapping (value="/getcategories" ,method=RequestMethod.GET )
public Map<String, Object> getAllCategory(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                          @RequestParam(value = "limit", required = false, defaultValue = "4") int limit){
    Pagination pagination = new Pagination(page, limit);
    pagination.setPage(page);
    pagination.setLimit(limit);

    pagination.setTotalCount(categoryService.countAllCategory());
    pagination.setTotalPages(pagination.getTotalPages());
    Map<String, Object> result = new HashMap<>();
    List<Category> categoryDtos = categoryService.getData(pagination);
    if(categoryDtos.isEmpty()){
        result.put("Message", "No data in Database");
    }else {
        result.put("Message", "Get Data Successful");
    }
    result.put("Pagination", pagination);
    result.put("Data", categoryDtos);
    return result;
}

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> addCategory(@RequestBody Category category){
        Map<String,Object> result=new HashMap<>();
        categoryService.insert(category);
        result.put("Message","Post seccessfully");
        result.put("DATA",category);
        return result;
    }

    @PutMapping("/{id}")
    public Map<String,Object> updateCategory(@PathVariable int id,@RequestBody Category category){
        Map<String,Object> result=new HashMap<>();
        Category category1=categoryService.findid(id);
        if(category1==null){
            result.put("Message","id Not Found Update Unsuccessfully");
            result.put("Response id",404);
        }else
        {
            categoryService.update(category);
            result.put("Message","Update Successfully");
            result.put("Response id",200);
            result.put("DATA",category);
        }
        return result;
    }
    @DeleteMapping("/{id}")
    public Map<String,Object> deleteArticle(@PathVariable int id){
        Map<String,Object> result=new HashMap<>();
        Category category1=categoryService.findid(id);
        if(category1==null){
            result.put("Message","id Not Found Delete Unsuccessfully");
            result.put("Response id",404);
        }else{
            categoryService.delete(id);
            result.put("Message","Delete Successfully");
            result.put("Response Code",200);
        }
        return result;
    }
    @GetMapping("{title}")
    public Map<String, Object> searchCategoryByTitle(@PathVariable String title){
        Map<String, Object> result = new HashMap<>();
        Category category = categoryService.getbytitle(title);
        try{
            if (category==null){
                result.put("Message", "Title of category Not Found");
                result.put("Response Code", "404");
            }else{
                categoryService.getbytitle(title);
                result.put("Data", category);
                result.put("Message", "Search Title of category Successfully");
                result.put("Response Code","200");
            }
            }
            catch (Exception e){
            e.getMessage();
        }
        return result;
    }
    @GetMapping("id/{id}")
    public Category getById(@PathVariable int id){
        return categoryService.getbyid(id);
    }

}

