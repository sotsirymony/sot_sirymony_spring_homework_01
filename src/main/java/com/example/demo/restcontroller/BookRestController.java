package com.example.demo.restcontroller;

import com.example.demo.Utilities.Pagination;
import com.example.demo.repository.DTO.Book;
import com.example.demo.repository.DTO.Category;
import com.example.demo.service.BookService;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/books")
public class BookRestController {

    private BookService bookService;
    private CategoryService categoryService;
    @Autowired
    public void setArticleService(BookService  bookService)
    {
        this.bookService = bookService;
    }
    @Autowired
    public void setArticleService(CategoryService  categoryService)
    {
        this.categoryService = categoryService;
    }


    @RequestMapping(method= RequestMethod.GET)
    public Map<String,Object> getArticle(){
        Map<String,Object> result=new HashMap<>();
        List<Book> book=bookService.findAll();
        if(book.isEmpty()){
            result.put("Message","No data in database");
        }else{
            result.put("Message","get seccessfully");
        }
        result.put("DATA",book);
        return result;
   }

   @RequestMapping (value="/getbooks" ,method=RequestMethod.GET )
public Map<String, Object> getAllData(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                      @RequestParam(value = "limit", required = false, defaultValue = "4") int limit) {
    Pagination pagination = new Pagination(page, limit);
    pagination.setPage(page);
    pagination.setLimit(limit);

    pagination.setTotalCount(bookService.coutAllBook());
    pagination.setTotalPages(pagination.getTotalPages());
    Map<String, Object> result = new HashMap<>();
    List<Book> bookDtos = bookService.getData(pagination);
    if (bookDtos.isEmpty()) {
        result.put("Message", "No data in Database");
    } else {
        result.put("Message", "Get Data Successful");
    }
    result.put("Pagination", pagination);
    result.put("Data", bookDtos);
    return result;
}

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> addArticle(@RequestBody Book book){
        Map<String,Object> result=new HashMap<>();
        bookService.insert(book);
        result.put("Message","Post seccessfully");
        result.put("DATA",book);
        return result;
    }

    @PutMapping("/{id}")
    public Map<String,Object> updateArticle(@PathVariable int id,@RequestBody Book book){
        Map<String,Object> result=new HashMap<>();
        Book book1=bookService.findid(id);
        if(book1==null){
            result.put("Message","id Not Found Update Unsuccessfully");
            result.put("Response id",404);
        }else
        {
            bookService.update(book);
            result.put("Message","Update Successfully");
            result.put("Response id",200);
            result.put("DATA",book);
        }
        return result;
    }

    @DeleteMapping("/{id}")
    public Map<String,Object> deleteArticle(@PathVariable int id){
        Map<String,Object> result=new HashMap<>();
        Book article1=bookService.findid(id);
        if(article1==null){
            result.put("Message","id Not Found Delete Unsuccessfully");
            result.put("Response id",404);
        }else{
            bookService.delete(id);
            result.put("Message","Delete Successfully");
            result.put("Response Code",200);
        }
        return result;
    }



    @GetMapping("{title}")
    public Map<String, Object> searchBookByTitle(@PathVariable String title){
        Map<String, Object> result = new HashMap<>();
        Book book = bookService.getbytitle(title);
        try{
            if (book==null){
                result.put("Message", "Title of Book Not Found");
                result.put("Response Code", "404");
            }else{
                bookService.getbytitle(title);
                result.put("Data", book);
                result.put("Message", "Search Title of Book Successfully");
                result.put("Response Code", "200");
            }
        }catch (Exception e){
            e.getMessage();
        }
        return result;
    }
    @GetMapping("/category/{title}")
    public Map<String, Object> searchcategoryByTitle(@PathVariable String title){
        Map<String, Object> result = new HashMap<>();
        Category book = categoryService.getbytitle(title);
        try{
            if (book==null){
                result.put("Message", "Title of category Not Found");
                result.put("Response Code", "404");
            }else{
                bookService.getbytitle(title);
                result.put("Data", book);
                result.put("Message", "Search Title of category Successfully");
                result.put("Response Code", "200");
            }
        }catch (Exception e){
            e.getMessage();
        }
        return result;
    }
    @GetMapping("id={id}")
    public Book getById(@PathVariable int id){
        return bookService.getbyid(id);
    }
}
