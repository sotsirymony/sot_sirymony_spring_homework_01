package com.example.demo.repository;

import com.example.demo.Utilities.Pagination;
import com.example.demo.repository.DTO.Book;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {
    @Update("UPDATE tb_books SET title=#{title},author=#{author},description=#{description},thumbnail=#{thumbnail},category_id=£{category_id} WHERE id=#{id}")
    boolean update(Book book);
    @Insert("INSERT INTO tb_books (id,title,author,description,thumbnail,category_id) VALUES (#{id},#{title},#{author},#{description},#{thumbnail},#{category_id})")
    boolean insert(Book book);
    @Select("SELECT * FROM tb_books ORDER BY id ASC LIMIT #{pagination.limit} OFFSET #{pagination.offset}")
    @Results({
            @Result(column = "category_id", property = "categoryDto",many = @Many(select = "selectByCateId"))
    })

    List<Book> getData(@Param("pagination") Pagination pagination);


    @Delete("DELETE FROM tb_books WHERE id=#{id}")
    boolean delete(int id);
    @Select("SELECT id FROM tb_books WHERE id=#{id}")
    Book findid(int id);
    @Select("SELECT id,title,author,description,thumbnail,category_id FROM tb_books WHERE id=#{id}")
    Book getbyid(int id);
    @Select("SELECT id,title,author,description,thumbnail,category_id FROM tb_books WHERE title=#{title}")
    Book getbytitle(String title);
    @Select("SELECT COUNT(id) FROM tb_books")
    int coutAllBook();

    @Select("SELECT * FROM tb_books")
    List<Book> findAll();


}
