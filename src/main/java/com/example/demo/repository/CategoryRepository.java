package com.example.demo.repository;

import com.example.demo.Utilities.Pagination;
import com.example.demo.repository.DTO.Book;
import com.example.demo.repository.DTO.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CategoryRepository {
    @Update("UPDATE tb_categories SET title=#{title} WHERE id=#{id}")
    boolean update(Category catory);
    @Insert("INSERT INTO tb_categories (id,title) VALUES (#{id},#{title})")
    boolean insert(Category category);
    @Select("SELECT * FROM tb_categories ORDER BY id ASC LIMIT #{pagination.limit} OFFSET #{pagination.offset}")
    List<Category> getData(@Param("pagination") Pagination pagination);
    @Delete("DELETE FROM tb_categories WHERE id=#{id}")
    boolean delete(int id);
    @Select("SELECT id FROM tb_categories WHERE id=#{id}")
    Category findid(int id);
    @Select("SELECT id,title FROM tb_categories WHERE id=#{id}")
    Category getbyid(int id);
    @Select("SELECT id,title FROM tb_categories WHERE title=#{title}")
    Category getbytitle(String title);
    @Select("SELECT COUNT(id) FROM tb_categories")
    int coutAllCategories();

    @Select("SELECT * FROM tb_categories")
    List<Category> findAll();

}