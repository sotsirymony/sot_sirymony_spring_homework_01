package com.example.demo.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class FileUploadConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/image/**").addResourceLocations("file:\\D:\\HRD\\1.HRD\\1.Lesson\\Lesson\\8.Spring\\Sot_Sirymony_Book_Management\\src\\main\\java\\com\\example\\demo\\Image\\");
    }
}
